  <div id="footer">
    <div class="contents clearfix">
    
      <div class="foot-cont">
        <p><a href="" class="ro-btn"><img src="/common/images/foot-logo.png" alt="サン・アクエリア"></a></p>
        <p><img src="/common/images/img-tel-foot.png" alt="06-6755-1165"></p>
        <p><a href="" class="ro-btn"><img src="/common/images/btn-foot-contact.png" alt="お問い合わせはこちら"></a></p>
      </div>
      
      <div class="foot-links">
        <ul>
          <li>
            <p>法人案内</p>
              <ul>
                <li><a href="">施設紹介</a></li>
                <li><a href="">私たちの想い</a></li>
                <li><a href="">マインド</a></li>
              </ul>
          </li>
          
          <li>
            <p>サービス案内</p>
              <ul>
                <li><a href="">特別養護老人ホーム</a></li>
                <li><a href="">デイサービス</a></li>
                <li><a href="">ショートサービス</a></li>
                <li><a href="">訪問介護</a></li>
                <li><a href="">居宅介護支援</a></li>
              </ul>
          </li>
                
          <li>
            <p><a href="">ご家族の方へ</a></p>
            <p class="mt20"><a href="">採用情報</a></p>
              <ul>
                <li><a href="">施設長メッセージ</a></li>
                <li><a href="">対談</a></li>
                <li><a href="">募集要項</a></li>
              </ul>
          </li>
                             
          <li>
            <p><a href="">スタッフブログ</a></p>
          </li>      
        </ul>
      </div>
    </div>
    <p class="copy">© 2015 サン・アクエリア All rights reserved.</p>
  </div><!-- /#footer -->
    
  