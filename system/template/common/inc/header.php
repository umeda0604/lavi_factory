  <div id="header">
    <div class="head-contents">
      <h1>大阪・生野の社会福祉法人 クローバ－会 サン・アクエリア</h1>
      
      <div class="head-inr">
        <div class="head-links01 clearfix">
          <ul>
            <li><img src="/common/images/img-tel.png" alt="06-6755-1165"></li>
            <li><a href="/inquiry/" class="ro-btn"><img src="/common/images/btn-contact.png" alt="お問い合わせはこちら"></a></li>
          </ul>
        </div>
        <div class="clearfix headmaincont">
          <p><a href="/"><img src="/common/images/logo.png" alt="社会福祉法人 サン・アクエリア"></a></p>
          <ul class="menu clearfix" id="g-navi">
            <li>
              <p><img src="/common/images/gnavi-01.png" alt="法人案内"></p>
              <ul>
                <li><a href="/institution/" class="ro-btn">施設紹介</a></li>
                <li><a href="/institution/concept.php" class="ro-btn">コンセプト</a></li>
                <li class="none-line"><a href="/institution/mind.php" class="ro-btn">マインド</a></li>
              </ul>
            </li>
            
            <li>
              <p><img src="/common/images/gnavi-02.png" alt="サービス内容"></p>
              <ul>
                <li><a href="/service/" class="ro-btn">特別養護老人ホーム</a></li>
                <li><a href="/service/adultdaycare.php" class="ro-btn">デイサービス</a></li>
                <li><a href="/service/short-termstay.php" class="ro-btn">ショートサービス</a></li>
                <li><a href="/service/homevisitcare.php" class="ro-btn">訪問介護</a></li>
                <li class="none-line"><a href="/service/homenursing.php" class="ro-btn">居宅介護支援</a></li>
              </ul>
            </li>            
            
            
            <li>
              <a href="/q_a/"><img src="/common/images/gnavi-03.png" alt="ご家族の方へ"></a>
            </li>               
            
            
            <li>
              <a href="/recruit/"><img src="/common/images/gnavi-04.png" alt="採用情報"></a>
            </li>            
            
            
            <li>
              <a href="/blog/"><img src="/common/images/gnavi-05.png" alt="スタッフブログ"></a>
            </li>               
          
          
          </ul>
      </div><!-- /.headmaincont -->
      </div><!-- /.head-inr -->
    </div><!-- ./head-contents -->
  </div><!-- /#header -->