/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// Define changes to default configuration here. For example:
	config.language = 'ja';
	// config.uiColor = '#AADC6E';

/** ALL **/
	CKEDITOR.config.toolbar = [
		['Source','-','Save','NewPage','Preview','-','Templates']
		,['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print','SpellChecker']
		,['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat']
		,['Form','Checkbox','Radio','TextField','Textarea','Select','Button','ImageButton','HiddenField']
		,'/'
		,['Bold','Italic','Underline','Strike','-','Subscript','Superscript']
		,['NumberedList','BulletedList','-','Outdent','Indent','Blockquote']
		,['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
		,['Link','Unlink','Anchor']
		,['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak']
		,'/'
		,['Styles','Format','Font','FontSize']
		,['TextColor','BGColor']
		,['ShowBlocks']
		];

	config.format_tags = 'p;h2;h3;h4;h5;h6;pre;div';
	/*config.format_h1 = { element : 'h1', attributes : { 'class' : 'ckh_01' } };
	config.format_h2 = { element : 'h2', attributes : { 'class' : 'head02' } };
	config.format_h3 = { element : 'h3', attributes : { 'class' : 'head03' } };	*/

	CKEDITOR.config.font_names='ＭＳ Ｐゴシック;ＭＳ Ｐ明朝;ＭＳ ゴシック;ＭＳ 明朝;Arial/Arial, Helvetica, sans-serif;Comic Sans MS/Comic Sans MS, cursive;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif';

	// タグのフィルタリングを無効にして任意の記述を許す
 	config.allowedContent = true;

 	// 外部CSS読み込み
	//config.contentsCss = "/admin/css/template/edit.css";
	//config.contentsCss = ["/admin/css/template/edit.css", ""];

	//Enterキー押下時のタグ
	/*CKEDITOR.config.enterMode = CKEDITOR.ENTER_P;*/

	//Shift+Enter押下時のタグ
	//CKEDITOR.config.shiftEnterMode = CKEDITOR.ENTER_BR;
/*
	config.filebrowserBrowseUrl = '/admin/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = '/admin/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = '/admin/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = '/admin/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = '/admin/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = '/admin/kcfinder/upload.php?type=flash';
*/
	config.filebrowserBrowseUrl = '/system/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = '/system/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = '/system/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = '/system/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = '/system/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = '/system/kcfinder/upload.php?type=flash';

	//ダイアログ：カスタマイズ
	CKEDITOR.on( 'dialogDefinition', function( ev ) {
		//定義中のダイアログ種類名を取得する
		var dialogName = ev.data.name;

		//ダイアログ定義を取得する
		var dialogDefinition = ev.data.definition;

		//全ダイアログ共通してリサイズを無効にする
		dialogDefinition.resizable = CKEDITOR.DIALOG_RESIZE_NONE;
		//ダイアログ個別に指定する
		if ( dialogName == 'link' )
		{
			//縦横方向のリサイズを許可する
			dialogDefinition.resizable = CKEDITOR.DIALOG_RESIZE_BOTH;

			//ダイアログ初期表示サイズ
			dialogDefinition.width  = 380;
//			dialogDefinition.height = 480;
		}
	});

	CKEDITOR.on('instanceReady', function(ev) {
	    ev.editor.dataProcessor.writer.indentationChars = '';
	    // 処理対象タグ
	    var tags = ['div',
	                'h1','h2','h3','h4','h5','h6',
	                'p',
	                'ul','ol','li','dl','dt','dd',
	                'table','thead','tbody','tfoot','tr','th','td',
	                'pre', 'address'];

	    for (var key in tags) {
	        ev.editor.dataProcessor.writer.setRules(tags[key], {
	            breakAfterOpen : false
	        });
	    }
	});

};
