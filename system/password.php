<?php
/*
 * Generated by phinagata 0.1.14033101
 * (C) dotAster Inc. All Rights Reserved.
 * http://qiq.to/phinagata/
 */

require_once 'program/config.php';  // %%tag_CONFIG_PHP_PATH%%
require_once 'auth_admin.php';

define('AUTH_TAG_ADMIN',  'auth_admin');

define('HASH_ALGO', 'sha256');

class
entryClass extends qiqFramework
{
  function
  entryClass()
  {
    $this->var = array(
      'mode',

      'seq',
      'flag',
      'login_name',
      'id',
      'password',
      'password_current',
      'password_new',
      'password_confirm',
      //'_password',

      'ok',
      'cancel',
    );

    $this->import($this->var);
    $this->set_html_dir(HTMLDIR. '/password');

    if (!$this->mode) $this->mode = 'form';

    $this->query = array();
    $this->query_string = _http_build_query($this->query);

    $this->session = $_SESSION['auth_admin'];
    $this->user_name = $this->session['login_name'];


    $array_week = array(
      0 => '日',
      1 => '月',
      2 => '火',
      3 => '水',
      4 => '木',
      5 => '金',
      6 => '土',
      );

    $this->today = date('Y.m.d', time());
    $this->day_of_week = $array_week[date('w', time())];
  }

  /* データ1件取得 */
  function
  get()
  {
    $this->db = qiqDB::connect(DSN);
    if (qiqDB::isError($this->db)) {
      $this->error = array('データベース接続エラー');
      return;
    }

    $SQL = sqlSelect(array(
      'column' => array(
        "seq",
        "flag",

        "login_name",
        "id",
        "password",

        "priority",
      ),
      'from'   => "login",
      'where'  => "seq = ". $this->db->quote($this->session['seq'], 'integer'),
    ));
    $entry = $this->db->extended->getRow($SQL, null, null, null, MDB2_FETCHMODE_ASSOC);
    if (qiqDB::isError($entry)) {
      $this->error = array('データベース問い合わせエラー');
      fblog($entry->toString());
      return;
    }
    $this->set($entry, $this->var);


  }

  function
  form()
  {
    if ($this->session['seq']) $this->get();
  }

  function
  prepare()
  {
  }

  function
  check()
  {
    if (!preg_match("/^[a-zA-Z0-9]+$/", $this->password)){
      $err[] = "パスワードは半角英数字のみで入力してください";
    }


    if ($err) {
      $this->error = $err;
      return 0;
    }

    return 1;
  }

  function
  confirm()
  {
    if ($this->cancel) {
      $this->mode = 'form';
      $this->view();
      return;
    }

    $this->prepare();

    if (!$this->check()) {
      $this->mode = 'form';
      return;
    }


    $this->flag -= 0;
  }

  function
  commit()
  {
    $this->mode = 'form';

    if ($this->cancel) {
      return;
    }

    $this->prepare();

    if (!$this->check()) {
      return;
    }

    $this->db = qiqDB::connect(DSN);
    if (qiqDB::isError($this->db)) {
      $this->error = array('データベース接続エラー');
      return;
    }


    $table = 'login';
    $types = array(
      'integer',
      'text', // login_name
      'text', // id
      // 'text',  // password
    );
    $column = array(
      'flag' => 1,
      'login_name' => $this->login_name,
      'id' => $this->id,
      // 'password' => hash(HASH_ALGO, $this->password),
    );
    if ($this->password) {
      $types[] = 'text';
      // PHP4用
      // $column['password'] = md5($this->_password);
      //$column['password'] = hash(HASH_ALGO, $this->_password);
      $column['password'] = $this->_password;
    }
    if ($this->session['seq']) {
      $seq = $this->session['seq'];
      $mode = MDB2_AUTOQUERY_UPDATE;
      $cond = "seq = ". $this->db->quote($this->seq, 'integer');
    } else {
      $mode = MDB2_AUTOQUERY_INSERT;
      $cond = null;
      $types[] = 'timestamp';
      $column['issue'] = date('Y/n/j G:i:s');
    }
    $res = $this->db->extended->autoExecute($table, $column,
              $mode,
              $cond,
              $types);
    if (qiqDB::isError($res)) {
      $this->error = array('データベース問合せエラー');
      fblog($res->toString(), __LINE__);
      return;
    }
    if (!$seq) {
      $seq = $this->db->lastInsertID();
      $res = $this->db->extended->autoExecute($table,
                array('priority' => $seq),
                MDB2_AUTOQUERY_UPDATE,
                "seq = ". $this->db->quote($seq, 'integer'),
                array('integer'));
      if (qiqDB::isError($res)) {
  $this->error = array('データベース問合せエラー');
  fblog($res->toString(), __LINE__);
  return;
      }
    }

    $this->db = qiqDB::connect(DSN);
    if (qiqDB::isError($this->db)) {
      $this->error = array('データベース接続エラー');
      return;
    }

    $SQL = sqlSelect(array(
      'column' => '*',
      'from'   => "login",
      'where'  => "seq = ". $this->db->quote($this->session['seq'], 'integer'),
    ));
    $entry = $this->db->extended->getRow($SQL, null, null, null, MDB2_FETCHMODE_ASSOC);
    if (qiqDB::isError($entry)) {
      $this->error = array('データベース問い合わせエラー');
      fblog($entry->toString());
      return;
    }

    $entry['_password'] = $this->password;
    session_regenerate_id(true);
    $_SESSION['auth_admin'] = $entry;

    //$this->mode = 'form';
    header("Location: ./password.php?mode=form");

  }

  function
  confirm_delete()
  {
    if ($this->session['seq']) $this->get();

  }

  /* データ削除 */
  function
  delete()
  {
    $this->mode = 'view';
    $this->view();

    if ($this->cancel) {
      return;
    }

    $this->db = qiqDB::connect(DSN);
    if (qiqDB::isError($this->db)) {
      $this->error = array('データベース接続エラー');
      return;
    }

    $res = $this->db->extended->autoExecute('login',
              null,
              MDB2_AUTOQUERY_DELETE,
              "seq = ". $this->db->quote($this->session['seq'], 'integer'));
    if (qiqDB::isError($res)) {
      $this->error = array('データベース問合せエラー');
      fblog($res->toString(), __LINE__);
      return;
    }

    $this->mode = 'view';
    $this->view();
  }

  function
  top()
  {
  }

  function
  up()
  {
    $this->get();
    priority(-1, $this->session['seq'], $this->priority, 'login');
    $this->mode = 'view';
    $this->view();
  }

  function
  down()
  {
    $this->get();
    priority(1, $this->session['seq'], $this->priority, 'login');
    $this->mode = 'view';
    $this->view();
  }

  function
  bottom()
  {
  }

  function
  dup()
  {
    $this->get();
    unset($this->_seq);
    $this->mode = 'form';
  }
}
$_auth = new Authentication();

$_entryClass = new entryClass();
$_entryClass->main('UTF-8');

// vi:ts=8 sw=2
?>
