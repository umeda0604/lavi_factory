-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2015 年 10 朁E19 日 06:03
-- サーバのバージョン： 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sun_aqa`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `seq` int(11) NOT NULL COMMENT '連番',
  `flag` int(11) DEFAULT '0' COMMENT '表示フラグ',
  `login_name` text COMMENT '名前',
  `id` text COMMENT 'ID',
  `password` text COMMENT 'パスワード',
  `priority` int(11) DEFAULT '0' COMMENT '表示順',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `issue` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '登録日時'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='ログイン情報';

--
-- テーブルのデータのダンプ `login`
--

INSERT INTO `login` (`seq`, `flag`, `login_name`, `id`, `password`, `priority`, `updated`, `issue`) VALUES
(1, 1, 'admin', 'queserser', 'c7d3febabf0989853f1b7d2a1812b146cd58570105733ac3534fb5fcdba9ac2a', 0, '2014-06-16 09:37:05', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`seq`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '連番',AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
