-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2015 年 10 朁E19 日 06:03
-- サーバのバージョン： 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sun_aqa`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `seq` int(11) NOT NULL COMMENT '連番',
  `flag` int(11) DEFAULT '0' COMMENT '表示フラグ',
  `publish` datetime DEFAULT NULL COMMENT '作成日',
  `title` text COMMENT '記事見出し',
  `body` text COMMENT '詳細記事',
  `priority` int(11) DEFAULT '0' COMMENT '表示順',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `issue` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '登録日時'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='新着情報';

--
-- テーブルのデータのダンプ `news`
--

INSERT INTO `news` (`seq`, `flag`, `publish`, `title`, `body`, `priority`, `updated`, `issue`) VALUES
(9, 1, '2015-10-19 00:00:00', 'TEST', '<p>TESTTEST</p>\r\n\r\n<p>TESTTEST</p>\r\n', 9, '2015-10-19 01:51:25', '2015-10-18 18:51:24'),
(10, 1, '2015-10-19 00:00:00', 'TEST02', '<p>TEST02</p>\r\n\r\n<p>TEST02</p>\r\n\r\n<p>TEST02</p>\r\n', 10, '2015-10-19 02:25:09', '2015-10-18 19:25:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`seq`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '連番',AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
