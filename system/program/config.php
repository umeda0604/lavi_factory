<?php

define('ENV_PRODUCTION',	false);	// 本番環境かどうか

// .htaccessが使えない場合
mb_language('Japanese');
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
ob_start('mb_output_handler');

////////////////////////////////////////////////////////////////////////

/* 管理画面情報 */
define('SITENAME',  'ラヴィ・ファクトリー');	// %%tag_SITENAME%%
define('COPYRIGHT', 'Copyright © ' . date( 'Y' ) . ' ラヴィ・ファクトリー All rights reserved.');	// %%tag_COPYRIGHT%%

/* メール関連 */
define('FROMNAME',    'ラヴィ・ファクトリー');    // %%tag_FROMNAME%%
if (defined('ENV_PRODUCTION') && ENV_PRODUCTION) {
  define('MAILFROM',    '差出人メールアドレス');  // %%tag_MAILFROM%%
  define('ADMINEMAIL',  '管理者メールアドレス');  // %%tag_ADMINEMAIL%%
} else {
  define('MAILFROM',    'test@queserser.co.jp');  // %%tag_MAILFROM%%
  define('ADMINEMAIL',  'umeda@queserser.co.jp');  // %%tag_ADMINEMAIL%%
  define('BCC',  'y_kimura@queserser.co.jp');  // %%tag_ADMINEMAIL%%
}

define('SUBJECT_CONTACT', 'お問い合わせありがとうございます');
define('SUBJECT_CONTACT_ADMIN', 'お問い合わせがありました');

/* データベース関連 */
define('DBTYPE',  'mysql');		// %%tag_DBTYPE%%
if (defined('ENV_PRODUCTION') && ENV_PRODUCTION) {
  define('DBHOST',  '');  // %%tag_DBHOST%%
  define('DBNAME',  '');    // %%tag_DBNAME%%
  define('DBUSER',  '');  // %%tag_DBUSER%%
  define('DBPASS',  ''); // %%tag_DBPASS%%
} else {
  define('DBHOST',  'mysql518.heteml.jp');  // %%tag_DBHOST%%
  define('DBNAME',  '_lavi');    // %%tag_DBNAME%%
  define('DBUSER',  '_lavi');  // %%tag_DBUSER%%
  define('DBPASS',  'queserser'); // %%tag_DBPASS%%
}
define('qDB_CLIENTCHARSET', 'utf8');

/* CK */
define('CK_KEY',    'RAW5yhLw'); // kcFinder用Key作成

/* 表示件数 */
define('NEWS_TOP_LIMIT',    6); // 表画面新着情報TOP一覧
define('NEWS_VIEW_LIMIT',    10); // 表画面新着情報一覧
define('ADMIN_NEWS_VIEW_LIMIT', 20);  // 管理画面新着情報一覧
define('BLOG_VIEW_LIMIT',    20); // 表画面ブログ情報一覧
define('ADMIN_BLOG_VIEW_LIMIT',    10); // 表画面ブログ情報TOP一覧
define('PRODUCT_VIEW_LIMIT',    10); // 表画面商品情報TOP一覧
define('ADMIN_PRODUCT_VIEW_LIMIT', 20);  // 管理画面商品情報一覧
// テスト環境IPアドレス(デバッグ表示)
define('qiqDebug_REMOTE_ADDR',	'/^118.243.35.xxx$/,/^192.168.1./');

////////////////////////////////////////////////////////////////////////

/* date */
define('SYSTEM_START_YEAR',	2015);
define('SEL_DEFAULT',		'▼選択');
define('SEL_YEAR_DEFAULT',	'----');
define('SEL_MONTH_DEFAULT',	'--');
define('SEL_DAY_DEFAULT',	'--');
define('SEL_HOUR_DEFAULT',	'--');
define('SEL_MINUTE_DEFAULT',	'--');
define('SEL_SECOND_DEFAULT',	'--');

/* dir */
define('DOCUMENT_ROOT',	$_SERVER['DOCUMENT_ROOT']);
define('BASEDIR',	dirname(__FILE__));	// %%tag_BASEDIR%%

////////////////////////////////////////////////////////////////////////

define('ENABLE_DEBUG',  0);
define('DEBUG',   0);
error_reporting(0);
if (ENABLE_DEBUG && DEBUG) ini_set('display_errors', 'On');

define('HTMLDIR', BASEDIR. '/html');
define('LIBDIR',  BASEDIR. '/lib');
define('INCDIR',  BASEDIR. '/inc');
define('MAILDIR', BASEDIR. '/mail');
define('DATADIR', BASEDIR. '/data');
define('TEMPLATE_DIR', DOCUMENT_ROOT. '/template');

define('MYSELF',  htmlspecialchars($_SERVER['PHP_SELF']));
define('DSN',	DBTYPE. '://'. DBUSER. ':'. DBPASS. '@'. DBHOST. '/'. DBNAME);

$inc = array(
  INCDIR,
  LIBDIR,
);

array_push($inc, ini_get('include_path'));
ini_set('include_path', join(':', $inc));

session_save_path(DATADIR . '/sess');
session_cache_limiter('');

require_once 'qiqDebug.php';
require_once 'qiqFramework.php';
require_once 'qiqForm.php';
require_once 'qiqCheckboxes.php';
require_once 'qiqPager.php';
require_once 'qiqDB.php';
require_once 'qiqSQL.php';
require_once 'reorder.php';

fbinit(qiqDebug_REMOTE_ADDR);

/* 正規表現 */
define('ALPHABET_CHECK', '/^[a-zA-Z]+$/');
define('PHONE_CHECK', '/^[0-9]{11}$|^[0-9]{10}$|^[0-9]{3}[0-9]{4}[0-9]{4}$|^[0-9]{2,5}[0-9]{1,4}[0-9]{4}$|^[0-9]{3} [0-9]{4} [0-9]{4}$|^[0-9]{2,5} [0-9]{1,4} [0-9]{4}$|^\d{1,5}-\d{1,5}-\d{1,5}$/');
define('PHONE_3_CHECK', '/^\d{2,5}$/');
define('MAIL_CHECK', '/^[-\w\._+]+@[-\w]+\.[-\w\.]+$/');
define('KATAKANA_CHECK', '/^([　 \t\r\n]|[ァ-ヶー]|[ー])+$/u');
define('HIRAKANA_CHECK', '/^([　 \t\r\n]|[ぁ-んー]|[ー])+$/u');
define('ZIP_CHECK', '/(^\d{3}\-\d{4}$)|(^\d{7}$)/');
define('ZIP_1_CHECK', '/^\d{3}$/');
define('ZIP_2_CHECK', '/^\d{4}$/');
define('NUMBER_CHECK', '/^[0-9]+$/');



$url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
define('MYURL', $url);
$parse_url_arr = parse_url ( $url );
$canonical = $parse_url_arr['scheme']. '://'. $parse_url_arr['host']. $parse_url_arr['path'];
define('MYCANONICAL', $canonical);

