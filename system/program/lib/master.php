<?php

// master.php: rev.13120601

function
get_label($table, $param = null)
{
  $default = array(
    'column'  => 'name',
    'where'   => null,
    'order'   => array('priority'),
    'default' => array(),
    'limit'   => null,
    'offset'  => null,
  );

  foreach ($default AS $key => $value) {
    $$key = ($param && array_key_exists($key, $param))? $param[$key]: $value;
  }

  $db = qiqDB::connect(DSN);
  if (qiqDB::isError($db)) {
    return $default;
  }

  $SQL = sqlSelect(array(
    'column'  => array(
      "seq",
      $column,
    ),
    'from'    => $table,
    'where'   => $where,
    'order'   => $order,
    'limit'   => $limit,
    'offset'  => $offset,
  ));
  $label = $db->extended->getAssoc($SQL, null, null, null, MDB2_FETCHMODE_ASSOC);
  if (qiqDB::isError($label)) $label = array(-1, $label->toString());

  return $default + $label;
}


function
get_info($table, $cond)
{
  $db = qiqDB::connect(DSN);
  if (qiqDB::isError($db)) {
    return;
  }

  $where = (is_int($cond))? "seq = $cond": $cond;

  $SQL = sqlSelect(array(
    'column' => '*',
    'from'   => $table,
    'where'  => $where,
  ));
  $info = $db->extended->getAssoc($SQL, null, null, null, MDB2_FETCHMODE_ASSOC);
  if (qiqDB::isError($info)) {
    return;
  }

  return $info;
}

?>
