<?php
/*
 * qiqImage.php: rev.14031401
 *
 * Copyright (c) dotAster Inc. <http://www.dotAster.com>
 *
 */

/*
 * [更新履歴]
 * 2014/03/14: DOCUMENT_ROOTを参照するように
 * 2014/03/11: type、convert を追加
 * 2013/02/18: resize=7 を追加
 */

if (!defined('DOCUMENT_ROOT')) {
  define('DOCUMENT_ROOT',	$_SERVER['DOCUMENT_ROOT']);
}

class
qiqImage
{
  var $name;		// フォームの値の名前
  var $dir;		// 保存ディレクトリ
  var $format;		// 名前生成フォーマット
  var $path;		// 保存ファイル名
  var $width;		// 最大幅
  var $height;		// 最大高さ
  var $resize;		// リサイズフラグ
			// 0: なし
			// 1: 固定
			// 2: 大きい時だけ縮小(アスペクト比保持)
			// 3: 幅固定
			// 4: 高さ固定
			// 5: 幅固定＋大きいときだけ縮小(アスペクト比保持)
			// 6: 高さ固定＋大きいときだけ縮小(アスペクト比保持)
			// 7: 指定された枠内に収める
  var $tmp_id;		// 一時ID
  var $tmp_dir;		// 一時保存ディレクトリ

  function
  qiqImage($array = null)
  {
    $this->name    = 'image';
    $this->type    = 'png';
    $this->dir     = '.';
    $this->format  = "%d.". $this->type;
    $this->resize  = 0;
    $this->tmp_dir = '.';
    $this->convert = true;	// 変換を強制する
    if (is_array($array)) {
      foreach ($array AS $key => $value) {
	$this->$key = $value;
      }
    }
    if (!$this->tmp_id) $this->tmp_id(true);

    /* ここで指定されたtmp_dir内での古いファイルを削除 */
    $dir = opendir($this->tmp_dir);
    while ($e = readdir($dir)) {
      if (substr($e, 0, strlen($this->name) + 4) != "{$this->name}_tmp") continue;
      $fname = sprintf('%s/%s', $this->tmp_dir, $e);
      $mtime = filemtime($fname);
      if (time() - $mtime > 3600) @unlink($fname);
    }
  }

  function
  dir($dir = '')
  {
    if ($dir) $this->dir = $dir;
    if (!file_exists($this->dir)) @mkdir($this->dir, 0777);
    return $this->dir;
  }

  function
  format()
  {
    $format = sprintf('%s/%s', $this->dir(), $this->format);
    $arg    = func_get_args();
    $this->path = vsprintf($format, $arg);
    return $this->path;
  }

  function
  path()
  {
    if (!$this->path) {
      $this->path = $this->format(func_get_args());
    }
    return $this->path;
  }

  function
  tmp_id($f = false)
  {
    if ($f) $this->tmp_id  = "{$this->name}_tmp". md5(uniqid(mt_rand(), true));
    return $this->tmp_id;
  }

  function
  tmp_path()
  {
    $ext = ($this->ext)? $this->ext: $this->type;
    return sprintf("%s/%s.%s", $this->tmp_dir, $this->tmp_id(), $ext);
  }

  function
  save_tmp($index = 0)
  {
    $file = $_FILES[$this->name]['tmp_name'];
    if (is_array($file)) $file = $_FILES[$this->name]['tmp_name'][$index];
    if (!$file || $file == 'none' || !filesize($file)) {
      return;
    }
    if (!is_uploaded_file($file)) {
      $this->error = "ファイルのアップロードに失敗しました";
      return;
    }
    $tmpimage = $this->tmp_path();
    if ($this->resize == -1) {	// 画像じゃない
      move_uploaded_file($file, $tmpimage);
      return;
    }
    $src = @imageCreateFromPNG($file);
    if ($src) {
      $type = 'png';
    } else {
      $src = @imageCreateFromJPEG($file);
      if ($src) {
	$type = 'jpg';
      } else {
	$src = @imageCreateFromGIF($file);
	if ($src) {
	  $type = 'gif';
	} else {
	  $this->error = "取り扱えない画像形式です。";
	  return;
	}
      }
    }
    $src_width = imageSX($src);
    $src_height = imageSY($src);
    $aspect_ratio = $src_height / $src_width;
    switch ($this->resize) {
    case 1:	// 固定
      if (!$this->convert &&
	  $src_width == $this->width && $src_height == $this->height &&
	  $type == $this->type) {
	move_uploaded_file($file, $tmpimage);
      } else {
	$dst = imageCreateTrueColor($this->width, $this->height);
	imageCopyResampled($dst, $src,
			   0, 0, 0, 0,
			   $this->width, $this->height, $src_width, $src_height);
      }
      break;
    case 2:	// 大きいときだけ縮小(アスペクト比保持)
      if (!$this->convert &&
	  $src_width <= $this->width && $src_height <= $this->height &&
	  $type == $this->type) {
	move_uploaded_file($file, $tmpimage);
      } else {
	if ($aspect_ratio > 1) {
	  if ($src_height < $this->height) {
	    $dst_width  = $src_width;
	    $dst_height = $src_height;
	  } else {
	    $dst_width  = $src_width * $this->height / $src_height;
	    $dst_height = $this->height;
	  }
	} else {
	  if ($src_width < $this->width) {
	    $dst_width  = $src_width;
	    $dst_height = $src_height;
	  } else {
	    $dst_width  = $this->width;
	    $dst_height = $src_height * $this->width / $src_width;
	  }
	}
	$dst = imageCreateTrueColor($dst_width, $dst_height);
	imageCopyResampled($dst, $src,
			   0, 0, 0, 0,
			   $dst_width, $dst_height, $src_width, $src_height);
      }
      break;
    case 3:	// 幅固定
      if (!$this->convert &&
	  $src_width == $this->width &&
	  $type == $this->type) {
	move_uploaded_file($file, $tmpimage);
      } else {
	$dst_width  = $this->width;
	$dst_height = $this->width * $aspect_ratio;
	$dst = imageCreateTrueColor($dst_width, $dst_height);
	imageCopyResampled($dst, $src,
			   0, 0, 0, 0,
			   $dst_width, $dst_height, $src_width, $src_height);
      }
      break;
    case 4:	// 高さ固定
      if (!$this->convert &&
	  $src_height == $this->height &&
	  $type == $this->type) {
	move_uploaded_file($file, $tmpimage);
      } else {
	$dst_width  = $this->height / $aspect_ratio;
	$dst_height = $this->height;
	$dst = imageCreateTrueColor($dst_width, $dst_height);
	imageCopyResampled($dst, $src,
			   0, 0, 0, 0,
			   $dst_width, $dst_height, $src_width, $src_height);
      }
      break;
    case 5:	// 幅固定＋大きいときだけ縮小(アスペクト比保持)
      if (!$this->convert &&
	$src_width <= $this->width &&
	$type == $this->type) {
	move_uploaded_file($file, $tmpimage);
      } else {
	$dst_width  = $this->width;
	$dst_height = $this->width * $aspect_ratio;
	$dst = imageCreateTrueColor($dst_width, $dst_height);
	imageCopyResampled($dst, $src,
			   0, 0, 0, 0,
			   $dst_width, $dst_height, $src_width, $src_height);
      }
      break;
    case 6:	// 高さ固定＋大きいときだけ縮小(アスペクト比保持)
      if (!$this->foce_convert &&
	$src_height <= $this->height &&
	$type == $this->type) {
	move_uploaded_file($file, $tmpimage);
      } else {
	$dst_width  = $this->height / $aspect_ratio;
	$dst_height = $this->height;
	$dst = imageCreateTrueColor($dst_width, $dst_height);
	imageCopyResampled($dst, $src,
			   0, 0, 0, 0,
			   $dst_width, $dst_height, $src_width, $src_height);
      }
      break;
    case 7:	// 指定された枠内に収める
      if (!$this->convert &&
	$src_width <= $this->width && $src_height <= $this->height &&
	$type == $this->type) {
	move_uploaded_file($file, $tmpimage);
      } else {
	$target_aspect = $this->height / $this->width;
	if ($aspect_ratio > $target_aspect) {
	  $dst_width  = $this->height / $aspect_ratio;
	  $dst_height = $this->height;
	} else {
	  $dst_width  = $this->width;
	  $dst_height = $this->width * $aspect_ratio;
	}
	$dst = imageCreateTrueColor($dst_width, $dst_height);
	imageCopyResampled($dst, $src,
			   0, 0, 0, 0,
			   $dst_width, $dst_height, $src_width, $src_height);
      }
      break;
    case 0:	// そのまま
    default:
      if (!$this->convert &&
	  $this->type == $type) {
	move_uploaded_file($file, $tmpimage);
      } else {
	$dst_width  = $src_width;
	$dst_height = $src_height;
	$dst = imageCreateTrueColor($dst_width, $dst_height);
	imageCopyResampled($dst, $src,
			   0, 0, 0, 0,
			   $dst_width, $dst_height, $src_width, $src_height);
      }
      break;
    }
    if ($dst) {
      imageInterlace($dst, 0);
      switch ($this->type) {
      case 'png':
	imagePNG($dst, $tmpimage);
	break;
      case 'jpg':
	imageJpeg($dst, $tmpimage, 95);
	break;
      case 'gif':
	imageGif($dst, $tmpimage);
	break;
      }
    }
  }

  function
  purge($complete = false)
  {
    $tmp = $this->tmp_path();
    $this->tmp_id(true);
    if (file_exists($tmp)) {
      if ($complete) {
	unlink($tmp);
      } else {
	$new = $this->tmp_path();
	rename($tmp, $new);
      }
    }
  }

  function
  save()
  {
    $tmp = $this->tmp_path();
    $img = $this->path();
    if ($this->delete) @unlink($img);
    if (file_exists($tmp)) rename($tmp, $img);
  }

  function
  delete()
  {
    @unlink($this->path());
  }

  function
  realpath()
  {
    return realpath($this->path());
  }

  function
  relpath()
  {
    return str_replace(DOCUMENT_ROOT, '', $this->realpath());
  }
}

// vi:ts=8 sw=2
?>
