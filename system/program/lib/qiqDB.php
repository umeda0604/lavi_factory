<?php
/*
 * qiqDB.php: rev.13120601
 *
 * Copyright (c) dotAster Inc. <http://www.dotAster.com>
 */

require_once 'MDB2.php';

/*
 * PHP4と5とで記述変える
 */

if (!defined('PHP_VERSION_ID')) {
  $version = explode('.', PHP_VERSION);
  define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

if (PHP_VERSION_ID < 50207 && is_array($version)) {
  if (!defined('PHP_MAJOR_VERSION'))   define('PHP_MAJOR_VERSION',   $version[0]);
  if (!defined('PHP_MINOR_VERSION'))   define('PHP_MINOR_VERSION',   $version[1]);
  if (!defined('PHP_RELEASE_VERSION')) define('PHP_RELEASE_VERSION', $version[2]);
}

class
qiqDB extends MDB2
{
  var $db;

  function
  connect($dsn, $opt = array())
  {
    if (PHP_MAJOR_VERSION >= 5) {
      $db = &MDB2::factory($dsn, $opt);
      $db->loadModule('Extended');

      if (defined('qDB_CLIENTCHARSET')) {
	$db->setCharset(qDB_CLIENTCHARSET);
      }

      return $db;
    } else {
      if (!$this->db) {
	$this->db = &MDB2::factory($dsn, $opt);
	$this->db->loadModule('Extended');
	if (defined('qDB_CLIENTCHARSET')) {
	  $this->db->setCharset(qDB_CLIENTCHARSET);
	}
      }
      return $this->db;
    }

  }
}

// vi:ts=8 sw=2
?>
