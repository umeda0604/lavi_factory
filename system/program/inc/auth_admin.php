<?php

define('AUTH_TAG_ADMIN',  'auth_admin');

define('HASH_ALGO', 'sha256');

class
Authentication extends qiqFramework
{
  function
  Authentication()
  {
    $this->clsvars = array(
      'memory',
      'login_name',
      'id',
      'password',
      'login',
      'logout',
    );
    $this->import($this->clsvars);

    $array_weekday = array(
      0 => '日',
      1 => '月',
      2 => '火',
      3 => '水',
      4 => '木',
      5 => '金',
      6 => '土',
      );

    $this->today = date('Y.m.d', time());
    $this->weekday = $array_weekday[date('w', time())];


    $this->error = array();

    $this->db = qiqDB::connect(DSN);
    if (qiqDB::isError($this->db)) {
      $this->error = 'データベースに接続できませんでした';
      return 0;
    }

    if ($_COOKIE['id']){
      $this->_id = $_COOKIE['id'];
    }

    session_start();

    if ($this->logout) $this->logout();

    if ($_SESSION[AUTH_TAG_ADMIN]['seq']) {
        $member = $this->get_contact(array(
        "seq = ". $this->db->quote($_SESSION[AUTH_TAG_ADMIN]['seq'], 'integer'),
        "id  = ". $this->db->quote($_SESSION[AUTH_TAG_ADMIN]['id'], 'text'),
        "password  = ". $this->db->quote($_SESSION[AUTH_TAG_ADMIN]['password'], 'text'),
      ));
      $_SESSION[AUTH_TAG_ADMIN]['today'] = $this->today;
      $_SESSION[AUTH_TAG_ADMIN]['weekday'] = $this->weekday;
      if ($member) return;
    }

    if ($this->login) {
      $id = trim(mb_convert_kana($this->id, 'as'));
      $password = trim(mb_convert_kana($this->password, 'as'));

      if (!$id) $err[] = 'IDを入力してください';
      if (!$password) $err[] = 'パスワードを入力してください';
      if ($err) {
        $this->error = '<ul><li>'. implode('</li><li>', $err). '</li></ul>';
      } else {

        $member = $this->get_contact(array(
          "id = ". $this->db->quote($id, 'text'),
        ));
        if (!$member) {
          $this->error = "<ul><li>アカウントが見つかりませんでした</li></ul>";
        //} else if ($member['password'] != hash(HASH_ALGO, $password)) {
        } else if ($member['password'] != $password) {
          $this->error = "<ul><li>パスワードが違います</li></ul>";
        } else {
          $member['_password'] = $password;
          $member['today'] = $this->today;
          $member['weekday'] = $this->weekday;
          session_regenerate_id(true);
          $_SESSION[AUTH_TAG_ADMIN] = $member;
          if ($this->memory){
            setcookie('id', $id);
          }else{
            setcookie('id', '', time() - 1);
          }
          return;
        }
/*　PHP4の場合

        $member = $this->get_contact(array(
          "id = ". $this->db->quote($id, 'text'),
        ));

        if (empty($member)) {
          $this->error = "<ul><li>アカウントが見つかりませんでした</li></ul>";
        } else if ($member['password'] != md5($password)) {
          $this->error = "<ul><li>パスワードが違います</li></ul>";
        } else {
          $member['_password'] = $password;
          session_regenerate_id(true);
          $_SESSION[AUTH_TAG_ADMIN] = $member;
          if ($this->memory){
            setcookie('id', $id);
          }else{
            setcookie('id', '', time() - 1);
          }
          return;
        }

*/
      }
    }
    $this->prompt();
  }

  function
  get_contact($where)
  {
   $SQL = sqlSelect(array(
      'column' => array(
  "login.*",
      ),
      'from'   => array(
  'login',
      ),
      'where'  => $where,
    ));

    $login = $this->db->extended->getRow($SQL, null, null, null, MDB2_FETCHMODE_ASSOC);
    if (qiqDB::isError($login)) {
      $this->error = 'データベース問合せエラー: '. $login->toString();
      return 0;
    }

    return $login;
  }

  function
  logout()
  {
    $this->clearSession();
    // PHP4の場合
    //$this->mb_include(HTMLDIR. '/login.html');
    header('Location: /system/');
    exit;
  }

  function
  prompt()
  {
    unset($_SESSION[AUTH_TAG_ADMIN]);
    $this->mb_include(HTMLDIR. '/login.html');
    exit;
  }
  function
  clearSession()
  {
    unset($_SESSION[AUTH_TAG_ADMIN]);
    setcookie(session_name(), '', time() - 3600, "/");
  }

}

function
get_login_contact($column)
{
  return $_SESSION[AUTH_TAG_ADMIN][$column];
}

function
is_login()
{
  return get_login_contact('seq');
}


// vi:ts=8 sw=2
?>
