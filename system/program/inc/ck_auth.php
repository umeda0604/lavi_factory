<?php

define('AUTH_TAG_ADMIN',  'auth_admin');

define('HASH_ALGO', 'sha256');

class
Authentication extends qiqFramework
{
  function
  Authentication()
  {
    $this->clsvars = array(
      'memory',
      'login_name',
      'id',
      'password',
    );
    $this->import($this->clsvars);

    $this->error = array();

    $this->db = qiqDB::connect(DSN);
    if (qiqDB::isError($this->db)) {
      $this->error = 'データベースに接続できませんでした';
      return 0;
    }

    session_start();

    if ($_SESSION[AUTH_TAG_ADMIN]['seq']) {
        $member = $this->get_contact(array(
        "seq = ". $this->db->quote($_SESSION[AUTH_TAG_ADMIN]['seq'], 'integer'),
        "id  = ". $this->db->quote($_SESSION[AUTH_TAG_ADMIN]['id'], 'text'),
        "password  = ". $this->db->quote($_SESSION[AUTH_TAG_ADMIN]['password'], 'text'),
      ));
      if ($member) return;
    } else {
      header('HTTP', true, 404);
      exit;
    }
  }

  function
  get_contact($where)
  {
   $SQL = sqlSelect(array(
      'column' => array(
  "login.*",
      ),
      'from'   => array(
  'login',
      ),
      'where'  => $where,
    ));

    $login = $this->db->extended->getRow($SQL, null, null, null, MDB2_FETCHMODE_ASSOC);
    if (qiqDB::isError($login)) {
      $this->error = 'データベース問合せエラー: '. $login->toString();
      return 0;
    }

    return $login;
  }
}

// vi:ts=8 sw=2
?>
