<?php

  // 指定キーワードでの配列化
  function
  _explode($value, $ext = null)
  {
    if (!$ext) $ext = "/,";
    if (strstr($value, $ext)){
      $array = explode($ext, $value);
      foreach ($array as $dummy => $data) {
        $_array[] = _str($data);
      }
    } else {
      $_array[] = _str($value);
    }
    return $_array;
  }

  // 指定キーワードでの配列化
  function
  _explode_int($value, $ext = null)
  {
    if (!$ext) $ext = "/,";
    if (strstr($value, $ext)){
      $array = explode($ext, $value);
      foreach ($array as $dummy => $data) {
        $_array[] = (int)$data;
      }
    } else {
      $_array[] = (int)$value;
    }
    return $_array;
  }

  // 指定キーワードでの文字列化
  function
  _implode($value, $ext = null)
  {
    if (!$ext) $ext = "/,";
    return implode($ext, $value);
  }

  // データベースから出す場合の配列のcheck_array化
  function
  _input_check($array) {
    foreach ($array as $key => $value) {
      $_value = (int)$value;
      $_array[$_value] = "on";
    }
    return $_array;
  }

  // データベースから出す場合の配列のテキスト化
  function
  number_text($array, $label, $ext = null) {
    if (!$ext) $ext = ',';
    foreach ($array as $key => $value) {
      $_value = (int)$value;
      $_array[] = $_value;
    }
    return implode($ext, $_array);
  }


  // データベースから出す場合の配列のラベル化
  function
  _label($array, $label, $ext = null) {
    if (!$ext) $ext = ',';
    foreach ($array as $key => $value) {
      $_value = (int)$value;
      $_array[] = $label[$_value];
    }
    return implode($ext, $_array);
  }

  // 確認画面での配列化
  function
  confirm_array($value, $ext = null) {
    if (!$ext) $ext = ',';
    if (strpos($value, $ext)){
      $array = explode($ext, $value);
      if (is_array($array)) {
        foreach ($array as $dummy => $data) {
          if ($data) {
            $_array[$data] = 'on';
          }
        }
      }
    } else {
      $_array[$value] = 'on';
    }
    return $_array;
  }

  // 確認画面での文字列化
  function
  confirm_name($array, $label, $ext = null) {
    if (!$ext) $ext = ',';
    if (is_array($array)) {
      foreach ($array as $dummy => $data) {
        if ($data) {
          $_array[] = $label[$data];
        }
      }
    }
    return implode($ext, $_array);
  }

  // checkbox用DB入力用文字列化 フォーマットあり
  function
  check_implode($value, $ext = null)
  {
    if (!$ext) $ext = "/,";
    foreach ($value as $key => $onoff) {
      $_value[] = spr_nb($key);
    }
    return implode($ext, $_value);
  }


  // checkbox用DB入力用文字列化 フォーマットなし
  function
  _check_array($array, $ext = null)
  {
    if (!$ext) $ext = "/,";
    if (is_array($array)) {
      foreach ($array as $key => $onoff) {
        $_array[] = $key;
      }
    }
    return implode($ext, $_array);
  }

  // checkbox用配列化
  function
  export_array($value, $ext = null) {
    if (!$ext) $ext = "/,";
    $array = explode($ext, $value);
    if (is_array($array)) {
      foreach ($array as $dummy => $data) {
        if ($data) {
          $_array[$data] = 'on';
        }
      }
    }
    return $_array;
  }

  // checkbox用文字列化
  function
  export_name($array, $label, $ext = null) {
    if (!$ext) $ext = "/,";
    if (is_array($array)) {
      foreach ($array as $dummy => $data) {
        if ($data) {
          $_array[] = $label[$data];
        }
      }
    }
    return implode($ext, $_array);
  }


?>